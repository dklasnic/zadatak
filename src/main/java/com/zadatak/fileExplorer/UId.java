package com.zadatak.fileExplorer;


import java.io.File;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.treegrid.TreeGrid;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;


@PageTitle("Proba")
@Route("/")
public class UId extends VerticalLayout {


	private static final long serialVersionUID = 1L;

	File folder =new File(System.getProperty("user.home"));
	
		public UId() {
			
			addTreeGrid();
			setSizeFull();
		}
	
	
		private void addTreeGrid() {
			TreeGrid<File> grid = new TreeGrid<>();
			DataFile dataFile = new DataFile();
			grid.setItems(dataFile.getFilesFromParent(folder),dataFile::getChild);
			grid.addHierarchyColumn(File::getName).setHeader("Naziv datoteke");
			grid.addColumn(File::getAbsolutePath).setHeader("Path");
			add(grid);
		}
		
	

}
