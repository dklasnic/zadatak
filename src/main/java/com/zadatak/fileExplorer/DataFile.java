package com.zadatak.fileExplorer;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
public class DataFile {
	
	
	
	
	public List<File> getFilesFromParent(File folder){
		return Arrays.asList(folder.listFiles());
	}
	
	public List<File>  getChild(File file){
		if (file == null || file.listFiles()==null)
					{return new ArrayList<File>();	}
		return Arrays.asList(file.listFiles());				
	}

}
